package jp.alhinc.moi_mahiro.calculate_sales;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class TestCalculateSalesBase {
	// 定数定義
	// パッケージ名
	private static final String packagePrefix = "jp.alhinc.";
	private static final String packageSuffix = ".calculate_sales.";
	// テストクラス名
	private static final String testClassName = "CalculateSales";
	// テストメソッド名
	private static final String testMethodName = "main";
	// 支店別出力ファイル名
	protected static final String outputBranchFileName = "branch.out";
	// 支店別出力内容確認用ファイル名
	protected static final String answerBranchFileName = "ans_branch.out";
	// 商品別出力ファイル名
	protected static final String outputCommodityFileName = "commodity.out";
	// 商品別出力内容確認用ファイル名
	protected static final String answerCommodityFileName = "ans_commodity.out";
	// システム改行文字
	protected static final String lineFeed = System.lineSeparator();
	// ファイル区切り文字
	protected static final String fileSeparator = File.separator;

	// テスト用
	static ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	protected static String testPath;
	protected static Class<?> clazz;
	protected static Method method;

	@Before
	public void resetTest() throws Exception {
		// 標準出力テスト用設定
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	}

	@BeforeClass
	public static void prepareTest() {
		try {
			// テストデータ格納パスを指定
			System.out.println("テストディレクトリを指定してください。");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			testPath = new String(in.readLine());

			// 研修生名を指定
			System.out.println("研修生名を入力してください。");
			in = new BufferedReader(new InputStreamReader(System.in));
			String traineeName = new String(in.readLine());

			// テストクラス・メソッドを定義
			clazz = Class.forName(packagePrefix + traineeName + packageSuffix + testClassName);
			method = clazz.getMethod(testMethodName, String[].class);

		} catch (Exception e) {
			fail("@BeforeClass異常終了");
		}
	}

	@AfterClass
	public static void endTest() {
		// 標準出力の設定を戻す
		System.setOut(System.out);
	}

	/**
	 * ファイルを削除する
	 *
	 * @param dirName	ディレクトリパス
	 * @param fileName	削除ファイル名
	 */
	protected void deletePreviousFile(String dirName, String fileName) {
		File delFile = new File(dirName, fileName);
		if (delFile.exists() && delFile.isFile()) {
			if (delFile.canWrite()) {
				// 読み取り専用でない場合削除
				delFile.delete();
			}
		}
	}

	/**
	 * テスト対象パスの生成（詳細版）
	 *
	 * @param pathArr
	 * @param size
	 * @param endsWithSeparator
	 * @return
	 */
	protected String[] createTestPath(String[] pathArr, int size, boolean endsWithSeparator) {
		if (size == 0) {
			return new String[0];
		}

		String[] ret = new String[size];
		StringBuilder sb = new StringBuilder(testPath);
		for (String str : pathArr) {
			sb.append(fileSeparator);
			sb.append(str);
		}
		if (endsWithSeparator) {
			sb.append(fileSeparator);
		}
		ret[0] = sb.toString();
		return ret;
	}

	/**
	 * テスト対象パスの生成（正常系簡易版）
	 *
	 * @param pathArr
	 * @return
	 */
	protected String[] createTestPath(String[] pathArr) {
		return createTestPath(pathArr, 1, false);
	}

	/**
	 * メッセージの整合性テスト
	 *
	 * @param args
	 *            テスト用ディレクトリパス
	 * @param expectMsg
	 *            期待出力メッセージ
	 */
	protected void assertOutputMesage(String[] args, String expectMsg) {
		try {
			method.invoke(clazz, new Object[] { args });
		} catch (final InvocationTargetException e) {
			fail("キャッチされない例外：" + e.getCause());
		} catch (IllegalAccessException | IllegalArgumentException e) {
			fail("アクセスできない" + e.toString());
		}
		assertThat(outContent.toString(), is(expectMsg + ("".equals(expectMsg) ? "" : lineFeed)));
	}

	/**
	 * 出力ファイルの整合性テスト
	 *
	 * @param testDir
	 * @param outputFileName
	 * @param answerFileName
	 * @param existFlg
	 */
	protected void assertOutputFile(String testDir, String outputFileName, String answerFileName, boolean existFlg) {
		// 出力ファイル取得
		File outputFile = new File(testDir, outputFileName);

		if (existFlg) {
			// 出力ファイルが存在し、中身が正しいことを確認
			if (outputFile.exists()) {
				// 中身の整合性チェック
				List<String> testSet = getFileContent(outputFile);
				List<String> ansSet = getFileContent(testDir, answerFileName);

				if (!assertFileContent(ansSet, testSet)) {
					fail(outputFileName + ": 出力ファイルの内容が正しくない");
				}
				return;
			}
			fail(outputFileName + ": ファイルが出力されていない");
		}
		// 出力ファイルが存在しないことを確認
		if (outputFile.exists()) {
			fail(outputFileName + ": 不正にファイルが出力されている");
		}
	}

	/**
	 * テキストファイル→ArrayListに格納（ファイル指定）
	 *
	 * @param file
	 * @return
	 */
	private List<String> getFileContent(File file) {
		List<String> ret = new ArrayList<>();
		if (file.exists()) {
			// テキストファイルを読み込む
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
					ret.add(line);
				}
			} catch (Exception e) {
				fail("ファイルの読み込みに失敗");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						fail("BufferedReaderのクローズに失敗");
					}
				}
			}
			return ret;
		}
		fail(file.getName() + "が存在しない");
		return ret;
	}

	/**
	 * テキストファイル→ArrayListに格納（ファイルパス指定）
	 *
	 * @param testDir
	 * @param fileName
	 * @return
	 */
	private List<String> getFileContent(String testDir, String fileName) {
		File file = new File(testDir, fileName);
		return getFileContent(file);
	}

	/**
	 * ファイル内容の比較
	 *
	 * @param ansList
	 * @param testList
	 * @return
	 */
	private boolean assertFileContent(List<String> ansList, List<String> testList) {
		if (ansList != null && testList != null) {
			if (ansList.size() == testList.size()) {
				ansList.removeAll(testList);
				if (ansList.size() == 0) {
					return true;
				}
				return false;
			}
			return false;
		}
		return false;
	}
}
