package jp.alhinc.moi_mahiro.calculate_sales;

import org.junit.Test;

public class TestCalculateSalesBranchOnly extends TestCalculateSalesBase {
	@Test
	public void コマンドライン引数なし() {
		String[] testDir = createTestPath(new String[] { "" }, 0, false);
		// deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		// assertOutputFile(testDir[0], false);
	}

	@Test
	public void コマンドライン引数複数() {
		String[] testDir = createTestPath(new String[] { "" }, 2, false);
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void パスの末尾がパスセパレータ() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "correct" }, 1, true);
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 正常系() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "correct" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 店舗名に支店を含まない() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "correct", "branch", "name" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 支店別の合計金額が最大値() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "correct", "sales", "amount", "branch" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 連番の最後がフォルダ() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "correct", "sales", "folder", "last" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイルが存在しない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "correct", "sales", "number", "none" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイルが１つ() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "correct", "sales", "number", "one" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 売上ファイルが複数() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "correct", "sales", "number", "toomany" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "");
		assertOutputFile(testDir[0], true);
	}

	@Test
	public void 支店コードにアルファベットを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "include", "alphabet" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードにひらがなを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "include", "hiragana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードに漢字を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "include", "kanji" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードにカタカナを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "include", "katakana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードに記号を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "include", "symbol" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードが２桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "length", "2" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店コードが４桁() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "branch", "code", "length", "4" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義ファイルが存在しない() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "branch", "exist" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルが存在しません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義ファイルの行単位の要素数が1() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "branch", "line", "1" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 支店定義ファイルの行単位の要素数が3() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "branch", "line", "3" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "支店定義ファイルのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上上限値を超える支店が存在するためファイル出力しない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "outfile", "does-not-output" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "合計金額が10桁を超えました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 出力ファイルロック() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "outfile", "locked" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		// assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの支店コードが存在しない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "branchcode" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000001.rcdの支店コードが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルにアルファベットを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "include", "alphabet" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルにひらがなを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "include", "hiragana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルに漢字を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "include", "kanji" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルにカタカナを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "include", "katakana" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルに記号を含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "include", "symbol" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "予期せぬエラーが発生しました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 店舗別売上が10桁超() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "amount", "over", "branch" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "合計金額が10桁を超えました");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが１行() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "sales", "lines", "1" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000004.rcdのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが3行() {
		String[] testDir = createTestPath(new String[] { "spec", "branch-fixtures", "error", "sales", "lines", "3" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "00000005.rcdのフォーマットが不正です");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルに異なる拡張子が存在() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "sequence", "extension" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルにディレクトリを含む() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "sequence", "folder" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルが連番でない() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "sequence", "lost" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの先頭にごみ() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "sequence", "prefix" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	@Test
	public void 売上ファイルの末尾にごみ() {
		String[] testDir = createTestPath(
				new String[] { "spec", "branch-fixtures", "error", "sales", "sequence", "suffix" });
		deletePreviousFile(testDir[0]);
		assertOutputMesage(testDir, "売上ファイル名が連番になっていません");
		assertOutputFile(testDir[0], false);
	}

	/**
	 * 前回出力ファイルの削除（支店）
	 *
	 * @param dirName
	 */
	private void deletePreviousFile(String dirName) {
		super.deletePreviousFile(dirName, outputBranchFileName);
	}

	/**
	 * 出力ファイルの整合性テスト（支店）
	 *
	 * @param dirName
	 * @param existFlg
	 */
	private void assertOutputFile(String dirName, boolean existFlg) {
		super.assertOutputFile(dirName, outputBranchFileName, answerBranchFileName, existFlg);
	}
}
