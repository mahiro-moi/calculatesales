package jp.alhinc.moi_mahiro.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {
		int cmdLinecount = args.length;
		if(cmdLinecount != 1 || cmdLinecount == 0) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		BufferedReader br =null;
		HashMap<String,String>mpCodeBranch = new HashMap<String,String>();
		HashMap<String,Long>mpCodeSales = new HashMap<String,Long>();
		String line = null;
		String path = args[0];
		if (!inPutDefine(path, mpCodeBranch , mpCodeSales , "branch.lst" ,"支店" , "^[0-9]{3}" )){
			return;
		}
		//売上ファイルの処理
		String fileNames = path;
		File files = new File(fileNames);
		File [] allData = files.listFiles();
		ArrayList <File> saleFiles = new ArrayList<>();
		for(File rcd : allData) {
			if(rcd.isFile()) {
				if(rcd.getName().matches("^\\d{8}.rcd$")) {
					saleFiles.add(rcd);
				}
			}
		}
		for(int i =0 ; i <saleFiles.size();i++) {
			File file =saleFiles.get(i);
			String saleFileName = file.getName();
			String saleFilenum = saleFileName.substring(0, 8);
			int Number =Integer.parseInt(saleFilenum);
			if(Number - i != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i = 0 ; i < saleFiles.size() ;i++) {
			File file =saleFiles.get(i);
		    ArrayList<String> simpleList = new ArrayList<>();
		    try{
		    	br = new BufferedReader(new FileReader(file));
			    while((line = br.readLine()) != null) {
			    	simpleList.add(line);
			    }
			    String saleFileName = file.getName();
			    if(simpleList.size() != 2) {
			    	System.out.println(saleFileName + "のフォーマットが不正です");
			    	return;
			    }else if(!mpCodeBranch.containsKey(simpleList.get(0))) {
			    	System.out.println(saleFileName + "の支店コードが不正です");
			    	return;
			    }else if(!simpleList.get(1).matches("^[0-9]*$")) {
			    	System.out.println("予期せぬエラーが発生しました");
			    	return;
			    }
			    Long hmSale = mpCodeSales.get(simpleList.get(0));
			    long listSale =new Long(simpleList.get(1));
			    Long sum = hmSale+listSale;
			    String stringSum = sum.toString();
			    if(stringSum.matches("^[0-9]{11,}")) {
			    	System.out.println("合計金額が10桁を超えました");
			    	return;
			    }else
			    	mpCodeSales.put(simpleList.get(0),sum);
		    }catch (IOException e){
		    	System.out.println("予期せぬエラーが発生しました");
			}finally {
				if (br != null) {
					try{
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
	    if(!outPut(path , mpCodeBranch , mpCodeSales ,"branch.out")) {  //ファイル出力メソッド
	    	return;
	    }
	}
	//支店定義ファイルの処理
	public static boolean inPutDefine(String path,HashMap<String,String>mpCodeBranch ,
			HashMap<String,Long>mpCodeSales , String fileName , String defineName , String regex ) {
		BufferedReader br = null;
		String line;
		try {
			File lstFile = new File (path,fileName);
			if (!lstFile.exists()) {
				System.out.println(defineName + "定義ファイルが存在しません");
				return false;
			}
			br = new BufferedReader(new FileReader(lstFile));
		    while ((line = br.readLine()) != null) {
		    	String [] branchLst = line.split(",");
		    	if(branchLst.length != 2) {
		    		System.out.println(defineName + "定義ファイルのフォーマットが不正です");
		    		return false;
		    	}
		    	String branchCode = branchLst[0];
	    		String branchName = branchLst[1];
		    	if(!branchCode.matches(regex)) {
		    		System.out.println(defineName + "定義ファイルのフォーマットが不正です");
		    		return false;
		    	}
		    	mpCodeBranch.put(branchCode,branchName);
		    	mpCodeSales.put(branchCode, (long) 0);
		    }
		}catch (IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if (br != null) {
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	//アウトプット
	public static boolean outPut(String path , HashMap<String,String>mpCodeBranch ,
			HashMap<String,Long>mpCodeSales , String fileName) {
		BufferedWriter bw =null;
	    try{
	    	File file = new File (path ,fileName);
	    	bw = new BufferedWriter(new FileWriter(file));
	    	for(Entry<String, String> entry : mpCodeBranch.entrySet()) {
	    		bw.write(entry.getKey() + "," + entry.getValue()+ "," + mpCodeSales.get(entry.getKey()));
	    		bw.newLine();
	    	}
	    }catch (IOException e){
	    	System.out.println("予期せぬエラーが発生しました");
	    	return false;
	    }finally {
			if (bw != null) {
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
	    }
	    return true;
	}
}